import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-movie-view',
  templateUrl: './movie-view.component.html',
  styleUrls: ['./movie-view.component.scss']
})
export class MovieViewComponent implements OnInit {
  movieList:any = [];
  constructor(http: HttpClient) {
     http.get('https://jsonplaceholder.typicode.com/photos')
    //http.get('http://www.omdbapi.com/?r=json&i=tt1285016&type=movie&plot=short&apikey=57f07424')
    .subscribe(response => {
      this.movieList = response;
      console.log(response);
    });
   }

  ngOnInit() {
  }

}
